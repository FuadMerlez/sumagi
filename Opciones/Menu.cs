﻿using crudModelCarts.DAL;
using crudModelCarts.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sumagi
{
    public partial class Menu {

        private static MonstruoDalArchivo monstruoDal = new MonstruoDalArchivo();


        private static void generadorMonstruos()
        {
            Random r = new Random();
            String[] tipo = { "fuego", "aire", "tierra", "agua" };
            Console.Clear();
            for (int i = 0; i < 50; i++)
            {
                Monstruo m = new Monstruo();
                m.IdCategoria = Convert.ToInt32(i + 1);
                m.Nombre = "monstruo" + (i + 1);
                m.Tipo = tipo[r.Next(0, tipo.Length)];
                m.PuntosV = Convert.ToInt32(r.Next(50, 500));
                m.PuntosA = Convert.ToInt32(r.Next(50, 500));
                monstruoDal.generadorMonstruo(m);
            }
            Console.WriteLine("los mounstros se crearon bien");
            opMenu();

        }

        private static void actualizarMonstruo(){
            Console.Clear();
            Console.WriteLine("Listado de Monstruo, Seleccione la ID de la Monstruo a Modificar");
            monstruoDal.listarMonstruo().ForEach(m => Console.WriteLine($"{m.IdCategoria} nombre = {m.Nombre})"));
            int index = Convert.ToInt32(Console.ReadLine());

            String Nombre;
            String tipo;
            int puntosV;
            int puntosA;

            Boolean esValido;

            do
            {
                Console.WriteLine("Ingrese su Nombre");
                Nombre = Console.ReadLine().Trim();
            } while (Nombre == string.Empty);
            do 
            {
                Console.WriteLine("Ingrese su Tipo");
                tipo = Console.ReadLine().Trim();
            } while (tipo == string.Empty);
            do
            {
                Console.WriteLine("Ingrese sus puntos de Vida");
                esValido = Int32.TryParse(Console.ReadLine().Trim(), out puntosV);
            } while (!esValido);
            do
            {
                Console.WriteLine("Ingrese sus puntos de Ataque");
                esValido = Int32.TryParse(Console.ReadLine().Trim(), out puntosA);
            } while (!esValido);

            Monstruo m = new Monstruo();
            m.Nombre = Nombre;
            m.Tipo = tipo;
            m.PuntosV = puntosV;
            m.PuntosA = puntosA;
            m.IdCategoria = index;

            monstruoDal.actualizarMonstruo(m);
        }

        private static void crearMonstruo(){

            Console.Clear();
            Console.WriteLine("Opciones agregar Monstruo!");

            String Nombre;
            String tipo;
            int puntosV;
            int puntosA;
            Boolean esValido;

            do
            {
                Console.WriteLine("Ingrese su Nombre");
                Nombre = Console.ReadLine().Trim();
            } while (Nombre == string.Empty);
            do
            {
                Console.WriteLine("Ingrese su Tipo");
                tipo = Console.ReadLine().Trim();
            } while (tipo == string.Empty);
            do
            {
                Console.WriteLine("Ingrese sus puntos de Vida");
                esValido = Int32.TryParse(Console.ReadLine().Trim(), out puntosV);
            } while (!esValido);
            do
            {
                Console.WriteLine("Ingrese sus puntos de Ataque");
                esValido = Int32.TryParse(Console.ReadLine().Trim(), out puntosA);
            } while (!esValido);
            Monstruo m = new Monstruo();
            m.Nombre = Nombre;
            m.Tipo = tipo;
            m.PuntosV = puntosV;
            m.PuntosA = puntosA;
            monstruoDal.agregarMonstruo(m);
        }

        private static int getRandom(int puntosV, int puntosA)
        {
            Random rnd = new Random();
            return rnd.Next(puntosV, puntosA);
        }

        public static void listarMonstruo() {

            monstruoDal.listarMonstruo().ForEach(m => Console.WriteLine($"{m.ToString()}"));
            
        }
        public static void eliminarMonstruo() {
            Console.WriteLine("Listado de MONSTRUOS, Seleccione el ID de la persona a eliminar");
            monstruoDal.listarMonstruo().ForEach(m => Console.WriteLine($"{m.IdCategoria}) nombre= {m.Nombre}"));
            monstruoDal.EliminarMonstruo(Convert.ToInt32(Console.ReadLine()));
        }
    }
}
