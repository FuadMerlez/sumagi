﻿using System;

namespace Sumagi
{
    public partial class Menu
    {
        static void Main(string[] args){
            while (opMenu()) ;
            Console.ReadKey();

        }

        private static bool opMenu()
        {
            Console.WriteLine("Menu de Duelo de Mounstros");
            Console.WriteLine("1- crear Mounstro");
            Console.WriteLine("2- Modificar Modificar Mounstro");
            Console.WriteLine("3- Listas Mounstros");
            Console.WriteLine("4- Eliminar Mounstros");
            Console.WriteLine("5- generandor de Monstruos");
            Console.WriteLine("0- Salir");
            Boolean Opcion = true;

            switch (Console.ReadLine()){
                case "1":
                   crearMonstruo();
                   break;
                case "2":
                    actualizarMonstruo();
                    break;
                case "3":
                    listarMonstruo();
                    break;
                case "4":
                    eliminarMonstruo();
                    break;
                case "5":
                    generadorMonstruos();
                    break;
                default:
                    Opcion = false;
                    break;
            }
            return Opcion;
        }
    }
}
